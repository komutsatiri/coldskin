﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu1 : MonoBehaviour {

	private Button 	playButton;
	private Button 	aboutButton;
	private Button 	exitButton;

	void playButtonClicked()
	{
		Application.LoadLevel("Scene02");
	}

	void exitButtonClicked()
	{
		Application.Quit ();
	}

	void aboutButtonClicked()
	{
		Application.LoadLevel("Scene03");
	}


	void OnGUI()
	{
		playButton = transform.FindChild ("PlayButton").GetComponent<Button> ();
		aboutButton = transform.FindChild ("AboutButton").GetComponent<Button> ();
		exitButton = transform.FindChild ("ExitButton").GetComponent<Button> ();


		playButton.onClick.AddListener (() => playButtonClicked ());
		exitButton.onClick.AddListener (() => exitButtonClicked ());
		aboutButton.onClick.AddListener (() => aboutButtonClicked ());
	


	}

	// Use this for initialization
	void Start () 
	{




	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
