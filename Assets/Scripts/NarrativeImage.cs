﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NarrativeImage : MonoBehaviour {

	GameObject Image;
	public Sprite[] images;

	Text txtPlayerA;
	Text txtPlayerB;
	Text txtPlayerC;
	string strPlayerA;
	string strPlayerB ;
	string strPlayerC;
	int hp;




	void OnGUI()
	{

		hp = 25;
		float boxHeight = 25.0f;
		float boxWidth = 150.0f;

		strPlayerA = "PlayerA :"+hp;
		strPlayerB = "Gunner1 :"+hp;
		strPlayerC = "Gunner2 :"+hp;

		GUI.Label (new Rect (-10.0f,5.0f,boxWidth,boxHeight), strPlayerA, "box");
		GUI.Label (new Rect (-10.0f,boxHeight * 1.2f,boxWidth,boxHeight), strPlayerB, "box");
		GUI.Label (new Rect (-10.0f,boxHeight * 2.2f,boxWidth,boxHeight), strPlayerC, "box");

	}
	// Use this for initialization
	void Start () 
	{
		images = Resources.LoadAll<Sprite> ("Images");
		Image = new GameObject();
		Image.AddComponent<SpriteRenderer>();

		Image.GetComponent<SpriteRenderer>().sprite = images[0];
	}

	public void changePicture(int randomFromReference)
	{


		if( randomFromReference >= 1 & randomFromReference <=5)
		{
			Image.GetComponent<SpriteRenderer>().sprite = images[0];
		}
		else if( randomFromReference >= 6 & randomFromReference <=10)
		{
			Image.GetComponent<SpriteRenderer>().sprite = images[1];
		}
		else
		{
			
			Image.GetComponent<SpriteRenderer>().sprite = images[2];
		}	
			
		
	



	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
